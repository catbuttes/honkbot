# HonkBot

HonkBot is a bit of a joke bot to change *everybody* on a server's nick to 'Honk' or a varient thereof.

Once invited and the role is correctly positioned, say `!honk` to honkify everybody in the server.

## Limitations:
- HonkBot can't change the server owners nick
- HonkBot can't change the nick of anybody with a role higher than HonkBot

## Requirements:
- HonkBot requires the "Manage Nicknames" permission
- HonkBot role should be above those whose nicks you wish it to change

## Invite link:
- https://discordapp.com/api/oauth2/authorize?client_id=714466485724119180&permissions=134217728&scope=bot

## I want to run HonkBot myself
Don't trust some internet rando to keep the bot running? Good for you! As you can probably see, HonkBot is available as a docker container. Just run `docker run -e HONKER_TOKEN={DISCORD BOT TOKEN HERE} registry.gitlab.com/catbuttes/honkbot` to fire up your own instance. You will need to create your own bot instance on Discord, but I'm sure you can figure that one out!