import discord
from discord.ext import commands


class HonkBot(commands.Bot):
    def __init__(self, config):
        super().__init__(command_prefix="h!")
        self.config = config

    async def on_member_join(self, member: discord.Member) -> None:
        await member.edit(nick=self.honkify(member.nick))

    def run(self, **kwargs):
        if len(kwargs) == 0:
            super().run(self.config["honker_token"])
        else:
            super().run(self.config["honker_token"], kwargs)

    def honkify(self, word: str):
        x = len(word)
        honk: str = "h" + ("o" * (x-3)) + "nk"
        honkified = ""

        for i in range(0, len(word)):
            if(word[i].isalpha()):
                if(word[i].isupper()):
                    honkified = honkified + honk[i].upper()
                else:
                    honkified = honkified + honk[i]
            else:
                honkified = honkified + word[i]

        return honkified

    async def honkify_guild(self, guild):
        members = await guild.fetch_members(limit=None).flatten()
        for member in members:
            honk = self.honkify(member.display_name)
            try:
                await member.edit(nick=honk)
            except Exception:
                print("Failed to update {0}".format(member.display_name))

    async def on_ready(self) -> None:
        for guild in self.guilds:
            await self.honkify_guild(guild)

    async def on_guild_join(self, guild):
        await self.honkify_guild(guild)

    async def on_message(self, message: discord.Message):
        if message.content.startswith("!honk"):
            print("HONK!")
            self.honkify_guild(message.guild)
