#!/usr/bin/env python3
import Honker
import os


def main():

    config = dict()
    config["honker_token"] = os.environ['HONKER_TOKEN']

    bot = Honker.HonkBot.HonkBot(config)
    bot.run()


if __name__ == "__main__":
    main()
